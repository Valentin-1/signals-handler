#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <signal.h>
#include <unistd.h>

#define BUF_SIZE 64

char msg[BUF_SIZE];

void handlerSig(int signo);
void handle_winch(int sig);

int main()
{
    struct sigaction sa;
    memset(&sa, 0, sizeof(struct sigaction));
    sa.sa_handler = handle_winch;
    sigaction(SIGWINCH, &sa, NULL);

	printf("use another console for testing this app\n");
	printf("print: kill <signal> <PID>\n");
	printf("INT or 2 or in this console: Ctrl + c\n");
	printf("USR1 or 10 for launch a surprise\n");
	printf("USR2 or 12 for close process\n\n");
    if (SIG_ERR == signal(SIGUSR1, handlerSig))
        perror("can't catch SIGUSR1");
    if (SIG_ERR == signal(SIGUSR2, handlerSig))
        perror("can't catch SIGUSR2");
    if (SIG_ERR == signal(SIGKILL, handlerSig))
        perror("can't catch SIGKILL");
    if (SIG_ERR == signal(SIGINT, handlerSig))
        perror("can't catch SIGINT");

	do{
		sleep(3);
	} while (1);

	return 1;
}

void handlerSig(int signo)
{
	switch (signo)
	{
		case SIGKILL:
			printf("SIGKILL\n"); //can't catch
		break;
		case SIGINT:
			printf("pressed Ctrl + c\n");
		break;
		case SIGUSR1:
			strcpy(msg, "в операционной системе выполняются гадкие дела, подождите");
			write(STDOUT_FILENO, msg, strlen(msg));
			do{
				sleep(1);
				write(STDOUT_FILENO, ".", 1);
			} while(1);
		break;
		case SIGUSR2:
			printf("kill self\n");
			exit(1);
		break;
	}
}

void handle_winch(int sig)
{
}